import { Action } from '@ngrx/store';
import { User } from '../../models/user.model';
export enum AuthActionTypes {
    LOGIN_REQUESTED = '[Auth] LOGIN Requested',
    LOGIN_SUCCESS = '[Auth] LOGIN Success',
    LOGIN_FAILED = '[Auth] LOGIN Failed',
    LOGOUT_REQUESTED = '[Auth] LOGOUT requested',
    LOGOUT_COMPLETED = '[Auth] LOGOUT completed',
    GET_USER_COMP_RIGHTS = '[Auth] Get User COMP Rights',
    GET_USER_COMP_RIGHTS_Succ = '[Auth] Get User COMP Rights Success',
    AUTH_ERROR = '[Auth] Error',
    CANCEL_LOGOUT_TIMER = '[Auth]  Cancel Logout Timer',
    SHOW_NOTIFiCATION = '[Auth]  Show Notification',
    SHOW_ERROR_MSG = '[Auth]  Show Error Message',

}
export class LoginRequested implements Action {
    readonly type = AuthActionTypes.LOGIN_REQUESTED;
    constructor(public payload: { username: string; password: string , logType: boolean }) {}
  }
  export class LoginSuccess implements Action {
    readonly type = AuthActionTypes.LOGIN_SUCCESS;
    constructor(public payload: User ) {}
  }
  export class LoginFailed implements Action {
    readonly type = AuthActionTypes.LOGIN_FAILED;
    constructor(public payload: { error: string, status: string }) {}
  }
  export class LogoutRequested implements Action {
    readonly type = AuthActionTypes.LOGOUT_REQUESTED;
    constructor(public payload: {logType: string, userName: string}) {}
  }
  export class LogoutCompleted implements Action {
    readonly type = AuthActionTypes.LOGOUT_COMPLETED;
  }
  export class GetUserCompRights implements Action {
    readonly type = AuthActionTypes.GET_USER_COMP_RIGHTS;
    constructor(public payload: {UserId: number, ComponentName: string} ) {}
  }
//   export class GetUserCompRightsSucc implements Action {
//     readonly type = AuthActionTypes.GET_USER_COMP_RIGHTS_Succ;
//     constructor(public payload: Right[] ) {}
//   }
  export class AuthError implements Action {
    readonly type = AuthActionTypes.AUTH_ERROR;
    constructor(public payload: { error: string }) {}
  }
  export class CancelLogOutTimer implements Action {
    readonly type = AuthActionTypes.CANCEL_LOGOUT_TIMER;
  }
  export class ShowNotification implements Action {
    readonly type = AuthActionTypes.SHOW_NOTIFiCATION;
    constructor(public payload: { msg: string, title: string, status: string }) {}
  }
  export class ShowErrorMsg implements Action {
    readonly type = AuthActionTypes.SHOW_ERROR_MSG;
    constructor(public payload: { msg?: string, title: string, status: string }) {}
  }
  export type Actions =
  | LoginRequested
  | LoginSuccess
  | LoginFailed
  | LogoutRequested
  | LogoutCompleted
  | AuthError
  | GetUserCompRights
  | CancelLogOutTimer
  | ShowNotification
  | ShowErrorMsg;
