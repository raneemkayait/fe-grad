import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UserState } from './user.state';

const UserFeatureState  = createFeatureSelector<UserState>('User');
export const getUser = createSelector(
    UserFeatureState,
    (state: UserState) => state.user,
  );
  export const getIsLoggedIn = createSelector(
    UserFeatureState,
    (state: UserState) => state.isLoggedIn,
  );
  export const getIsLoading = createSelector(
    UserFeatureState,
    (state: UserState) => state.isLoading,
  );
  export const getError = createSelector(
    UserFeatureState,
  (state: UserState) => state.error,
);
  export const getUserToken = createSelector(
    UserFeatureState,
    (state: UserState) => state.user?.token,
  );
  export const getUserphoto = createSelector(
    UserFeatureState,
    (state: UserState) => state.user.photoUrl,
  );

  export const getUserId = createSelector(
    UserFeatureState,
    (state: UserState) => state.user.id,
  );
  export const getPermissions = createSelector(
    UserFeatureState,
    (state: UserState) => state.user?.rights,
  );
 

  