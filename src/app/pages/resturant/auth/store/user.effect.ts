import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { AuthService } from '../../Services/auth.service';
import * as AuthActions from './user.actions';
import * as fromAuth from './user.selector';
import {
  map,
  mergeMap,
  catchError,
  tap,
  switchMap,

} from 'rxjs/operators';
import { iif, Observable, of } from 'rxjs';
import { Action, Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { User } from '../../models/user.model';
import { ErrorMsgComponentComponent } from '../../../components/error-msg-component/error-msg-component.component';
@Injectable()
export class UserEffect {


  constructor(private actions$: Actions,
    private loginService: AuthService,
    private router: Router,
    private dialogService: NbDialogService,
    private toastrService: NbToastrService,
    private store: Store) { }

  @Effect()
  Login$: Observable<Action> = this.actions$.pipe(
    ofType<AuthActions.LoginRequested>(
      AuthActions.AuthActionTypes.LOGIN_REQUESTED,
    ),
    map((action: AuthActions.LoginRequested) => action.payload),
    mergeMap((Person) =>
      this.loginService.login({ username: Person.username, password: Person.password }).pipe(
        switchMap(
          (token: string) => [

            new AuthActions.ShowNotification({ msg: '', title: 'Login Success', status: 'success' }),
            new AuthActions.LoginSuccess(new User(Person.username, Person.password, token)),
          ]
        ),

        tap((err) => console.log(err)),
      ),

    ),
  );

  //     @Effect()
  //   userRights$: Observable<Action> = this.actions$.pipe(
  //       ofType<AuthActions.GetUserCompRights>(
  //         AuthActions.AuthActionTypes.GET_USER_COMP_RIGHTS,
  //       ),
  //       map((action: AuthActions.GetUserCompRights) => action.payload),
  //       mergeMap((payload) =>
  //         this.rightService.getUserRightsByComponent(payload.ComponentName, payload.UserId).pipe(
  //           map(
  //             (userRights: Right[]) =>
  //                 new AuthActions.GetUserCompRightsSucc(userRights),
  //           ),
  //           catchError(err =>
  //             iif(() => err.status === 400, of(new AuthActions.AuthError(err)),
  //             of(new AuthActions.ShowErrorMsg(
  //              {title: 'ERROR ' + err.status, status: 'danger'}))
  //            ),
  //          ),
  //         ),
  //       ),
  //     );
  @Effect({ dispatch: false })
  LogInSuccess: Observable<any> = this.actions$.pipe(
    ofType<AuthActions.LoginSuccess>(
      AuthActions.AuthActionTypes.LOGIN_SUCCESS,
    ),
    tap(() => {
      this.router.navigate(['/pages/dashboard']);
    }),
  );
  @Effect({ dispatch: false })
  ShowNotifi: Observable<any> = this.actions$.pipe(
    ofType<AuthActions.ShowNotification>(
      AuthActions.AuthActionTypes.SHOW_NOTIFiCATION,
    ),
    map((action: AuthActions.ShowNotification) => action.payload),
    tap((payload) => {
      // console.log('ShowNotification');
      // console.log(payload);
      this.toastrService.show(payload.msg, payload.title,
        { status: payload.status, duration: 2500 });
    }),
  );
  @Effect({ dispatch: false })
  ShowErrorMsg: Observable<any> = this.actions$.pipe(
    ofType<AuthActions.ShowErrorMsg>(
      AuthActions.AuthActionTypes.SHOW_ERROR_MSG,
    ),
    map((action: AuthActions.ShowErrorMsg) => action.payload),
    tap((payload) => {
      //  console.log('ShowErrorMsg');
      //  console.log(payload);
      const dialogRef = this.dialogService.open(ErrorMsgComponentComponent,
        { hasBackdrop: false, context: payload.msg });
      dialogRef.componentRef.instance.title = payload.title;
      dialogRef.componentRef.instance.msg = payload.msg;
      dialogRef.componentRef.instance.status = payload.status;
    }),
  );
  @Effect()
  Logout$: Observable<Action> = this.actions$.pipe(
    ofType<AuthActions.LogoutRequested>(
      AuthActions.AuthActionTypes.LOGOUT_REQUESTED,
    ),
    
    map((action: AuthActions.LogoutRequested) => action.payload),
    mergeMap((payload) =>
      this.loginService.logOut().pipe(
        tap(() => {
          console.log('test');
          this.router.navigateByUrl('/auth/login')
        }),
        map(
          () =>
            new AuthActions.LogoutCompleted(),
        ),
        catchError(err => of(new AuthActions.AuthError(err))),
      ),
    ),
  );
  //   @Effect()
  //   extendApplicationTimeout$: Observable<Action> = this.actions$.pipe(
  //     switchMap( (action: Action ) => timer(this.APPLICATION_TIMEOUT_TIME).pipe(
  //       switchMap(() => this.store.select(fromAuth.getIsLoggedIn).pipe(
  //         tap((a) => {
  //           if (a) {
  //             this.dialogService.open(LogOutTimerDialogComponent, { closeOnBackdropClick: false , });
  //           }
  //         }),
  //       )),
  //       switchMap((a) => iif (() => a, timer(this.Dialog_TIMEOUT_TIME), null).pipe(
  //         withLatestFrom(this.store.select(fromAuth.getUser)),
  //         map((user) => new AuthActions.LogoutRequested({logType: 'System Logout',
  //           userName: user[1].userName})),
  //         )),
  //         takeUntil(this.actions$.pipe(ofType(AuthActions.AuthActionTypes.CANCEL_LOGOUT_TIMER),
  //         )),
  //     ),
  //   ),
  //   );
  // }
}