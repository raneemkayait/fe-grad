import { User } from '../../models/user.model';

export interface UserState {
    user: User | null;
    isLoggedIn: boolean;
    isLoading: boolean;
    rightsLoading: boolean;
    error: any;
  }
  export const userInitialState: UserState = {
    user: null,
    isLoggedIn: false,
    isLoading: true,
    error: null,
    rightsLoading: false,
  };
