import * as userActions from './User.actions';
import { userInitialState, UserState } from './user.state';
export function userReducer(
    state = userInitialState,
    action: userActions.Actions,
  ): UserState {
    switch (action.type) {
      case userActions.AuthActionTypes.LOGIN_SUCCESS: {
        const Userstate = {...state,
          user: action.payload,
          error: '',
          isLoggedIn: true,
          isLoading: false,
        };
        localStorage.setItem('user', JSON.stringify(Userstate));
        return Userstate;
      }
      case userActions.AuthActionTypes.LOGIN_FAILED: {
        return {
          ...state,
          error: action.payload.error,
        };
      }
    
      case userActions.AuthActionTypes.LOGOUT_COMPLETED: {
        localStorage.clear();
        sessionStorage.clear();
        return  {...state,
          user: state.user,
          error: '',
          isLoggedIn: false,
          isLoading: false,
        };
      }

      default:
        {
         if (state.user === null && localStorage.getItem('user')) {
            return JSON.parse(localStorage.getItem('user'));
          }
          return state;
        }
  }
}
