import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { UserState } from '../store/user.state';
import * as UserActions from '../store/user.actions';
import { Observable } from 'rxjs';
import * as fromUser from '../store/user.selector';
import { threadId } from 'worker_threads';
import { AppState } from '../../state/app-state';
@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  employee: boolean;
  submitted: boolean;
  error$: Observable<string>;
  constructor(private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.username = '';
    this.password = '';
    this.employee = false;
    this.error$ = this.store.pipe(select(fromUser.getError));
  }
  reSubmit() {
    this.submitted = false;
  }
  IsEmployee($event) {
    this.employee = $event;
    this.reSubmit();
  }
  onSubmit() {
    this.submitted = true;
    console.log(this.employee);
    this.store.dispatch(new UserActions.LoginRequested({
      username: this.username,
      password: this.password, logType: this.employee
    }));
  }
}

