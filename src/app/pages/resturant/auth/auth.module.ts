import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { AuthComponent } from './auth.component';

import { NbButtonModule, NbCheckboxModule } from '@nebular/theme';
import { SignUpComponent } from './sign-up/sign-up.component';
@NgModule({
  declarations: [
    AuthComponent,
    LoginComponent,
    SignUpComponent,
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    NbCheckboxModule,
    NbButtonModule,
  ],
  providers: [
    { provide: 'componentName', useFactory: () => 'login'},
    ],
})
export class AuthModule { }
