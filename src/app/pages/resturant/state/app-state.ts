import { ActionReducerMap } from '@ngrx/store';
import { UserEffect } from '../auth/store/user.effect';
import { userReducer } from '../auth/store/user.reducer';
import { UserState } from '../auth/store/user.state';
import  * as UserActions from '../auth/store/user.actions';


export interface AppState {
    User: UserState;
   

  }
  export const reducers: ActionReducerMap<AppState> = {
    User: userReducer
  };
  export const effects = [UserEffect];

  export function clearState(reducer) {
    return function (state, action) {
      if (action.type === UserActions.LogoutCompleted) {
        state = undefined;
      }
      return reducer(state, action);
    };
  }
