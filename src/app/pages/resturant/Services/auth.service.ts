import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl: string = environment.BaseUsrl + "auth/" + 'login';

  constructor(private http: HttpClient) { }
  public login(user): Observable<any> {
    return this.http.post(this.baseUrl, user,
      { responseType: 'text' });

  }
  public logOut(): Observable<any> {
    return of(true);
  }
}
