import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as fromAuth from '../auth/store/user.selector';
@Injectable({
  providedIn: 'root',
})
@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(public router: Router, private store: Store<any>)  {}
  canActivateChild(): Observable<boolean> {
    return this.canActivate();
  }
    canActivate(): Observable<boolean> {
      const isloggedIn$ = this.store.select(fromAuth.getIsLoggedIn);
      const user = localStorage.getItem('user');
      if (isloggedIn$) {
          return  new Observable<boolean>((observer) =>  { isloggedIn$.subscribe(isloggedIn => {
            if (isloggedIn && user !== null) {
              observer.next(true) ;
              observer.complete();
            }else {
                this.router.navigateByUrl('/auth/login');
                return false;
              }
         });
      });
    }
  }
}
