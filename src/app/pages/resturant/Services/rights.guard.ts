import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateChild, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as fromAuth from './../auth/store/user.selector';
import * as UserActions from '../auth/store/user.actions';
import { map, switchMap, take, tap } from 'rxjs/operators';
import { filter } from 'rxjs/operators';
import { RightsService } from './rights.service';

@Injectable({
  providedIn: 'root',
})
export class RightsGuard implements CanActivateChild {
  hasPerm: boolean = false;
  constructor(public router: Router, private store: Store<any>, private rightServic: RightsService)  {}
  canActivateChild(
    next: ActivatedRouteSnapshot): Observable<boolean> {
      const componentName = next.data['resource'];
      const requiredPermission = next.data['permission'];
      return this.getUserRights(componentName).pipe(
        switchMap(() => {
          return this.hasPermission(requiredPermission, componentName);
         // return of(this.hasPerm);
        },
        ),
      );
  }
  getUserRights(componentName: string): Observable<any> {
    let userCompRight: any[];
    return this.store
      .select(fromAuth.getUser).pipe(
       tap((user) => {
         if (user.rights.length > 0) {
          userCompRight = user.rights.find(c => c.componentName ===  componentName);
          if (userCompRight === undefined) {
            this.store.dispatch(new UserActions.GetUserCompRights(
              {
                UserId: user.id,
                ComponentName: componentName,
              }));
          }
         }else {
           this.router.navigateByUrl('pages/404');
         }
      }),
      filter(user => userCompRight === undefined ? false : true),
      take(1),
      );
  }
  hasPermission(permission, component): Observable<boolean> {
      return this.store.select(fromAuth.getPermissions).pipe(
        map(permissions => this.MultiplePermission(permission, permissions, component)),
        tap(result => {
          if (result === false) {
            console.log(permission);
            console.log(component);

            this.router.navigateByUrl('/pages/unauthorized');
          }
        }),
        take(1),
      );
  }
  MultiplePermission (reqPermssion, userPerm, component):  boolean {
    let res = false;
    for (const per of reqPermssion) {
      res = (userPerm.find(c => c.componentName === component)?.rights
      .find(p => p === per) === undefined ? false : true) === true || res ;
      // Add Right to DB
      if (userPerm.find(c => c.componentName === component)?.rights
      .find(p => p === per) === undefined) {
        this.rightServic.addRight({
          rightKey: per,
          rightclassif: component,
        }).subscribe(res => { });
      }
     
    }
    return  res;
  }
}
