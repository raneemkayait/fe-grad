import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

import { Right } from '../Models/right.model';
@Injectable({
  providedIn: 'root',
})
export class RightsService {
  private Url = environment.BaseUsrl + 'Rights';
  constructor(private http: HttpClient) { }
  getAllRights(): Observable<any[]> {
    return this.http.get<any[]>(this.Url + '/AllRights');
  }
  addRight(payload: Right): Observable<Right> {
    return this.http.post<Right>(this.Url, payload);
  }
  deleteRight(MissionTypeIdId: Number) {
    return this.http.delete(this.Url + '/' + MissionTypeIdId);
  }
  getRightById(MissionTypeId: number): Observable<Right> {
    return this.http.get<Right>(this.Url + '/right/' + MissionTypeId);
   }
   AllRightsUser(UserId: number): Observable<Right[]> {
    return this.http.get<Right[]>(this.Url + '/Userrights/' + UserId);
   }
   getUserRightsByComponent(componentName: string, userName: string): Observable<Right[]> {
    return this.http.get<Right[]>(this.Url + '/getUserRights' +
    '?userName=' + userName + '&componetName=' + componentName);
   }
   getAllPages(): Observable<string[]> {
    return this.http.get<string[]>(this.Url + '/pages');
   }
}
