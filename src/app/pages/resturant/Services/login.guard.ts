import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as fromAuth from '../auth/store/user.selector';

@Injectable({
  providedIn: 'root',
})
export class LoginGuard implements CanActivate {
  constructor(public router: Router, private store: Store<any>)  {}
  canActivate(): Observable<boolean> {
    const isloggedIn$ = this.store.select(fromAuth.getIsLoggedIn);
    const user = localStorage.getItem('user');

    if (isloggedIn$) {
        return  new Observable<boolean>((observer) =>  { isloggedIn$.subscribe(isloggedIn => {
          if (!isloggedIn || user === null) {
            observer.next(true) ;
            observer.complete();
          }else {
            var type = localStorage.getItem('loginType');
            if (type !== null && type === 'Employee') {
              this.router.navigateByUrl('/dashboard');
            }else {
              this.router.navigateByUrl('/pages/dashboard');
            }
              return false;
            }
       });
    });
  }
}
}
