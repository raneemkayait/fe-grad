export class User {
    username: string;
    password: string;
    role?: any;
    token?: string;
    id?: number;
    photoUrl?: any;
    rights?: any;
    constructor(username, password, token) {
        this.username = username;
        this.password = password;
        this.token = token;
    }
}
