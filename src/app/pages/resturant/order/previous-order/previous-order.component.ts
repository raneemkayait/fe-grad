import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'ngx-previous-order',
  templateUrl: './previous-order.component.html',
  styleUrls: ['./previous-order.component.scss']
})
export class PreviousOrderComponent implements OnInit {
  @ViewChild('item', { static: true }) accordion;

  toggle() {
    this.accordion.toggle();
  }
  constructor() { }
  

  ngOnInit(): void {
  }

}
