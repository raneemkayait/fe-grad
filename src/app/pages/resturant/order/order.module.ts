import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpcomingOrderComponent } from './upcoming-order/upcoming-order.component';
import { PreviousOrderComponent } from './previous-order/previous-order.component';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { OrderRoutingModule } from './order-routing.module';
import { NbAccordionModule, NbButtonModule, NbProgressBarModule } from '@nebular/theme';



@NgModule({
  declarations: [UpcomingOrderComponent, PreviousOrderComponent, OrderHistoryComponent],
  imports: [
    CommonModule,
    OrderRoutingModule,
    NbAccordionModule,
    NbButtonModule,
    NbProgressBarModule,

  ]
})
export class OrderModule { }
