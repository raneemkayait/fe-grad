import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-upcoming-order',
  templateUrl: './upcoming-order.component.html',
  styleUrls: ['./upcoming-order.component.scss']
})
export class UpcomingOrderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  value = 25;

  setValue(newValue) {
    this.value = Math.min(Math.max(newValue, 0), 100)
  }

  get status() {
    if (this.value <= 25) {
      return 'danger';
    } else if (this.value <= 50) {
      return 'warning';
    } else if (this.value <= 75) {
      return 'info';
    } else {
      return 'success';
    }
  }

}
