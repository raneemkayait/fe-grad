import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MealsComponent } from './meals/meals.component';
import { MenuComponent } from './menu/menu.component';
import { CategoriesComponent } from './categories/categories.component';
import { MenuRoutingModule } from './menu-routing.module';
import { IngredientListComponent } from './ingredient-list/ingredient-list.component';
import { IngredientComponent } from './ingredient/ingredient.component';
import { RecipeComponent } from './recipe/recipe.component';
import { RecipeListComponent } from './recipe-list/recipe-list.component';



@NgModule({
  declarations: [MealsComponent, MenuComponent, CategoriesComponent, IngredientListComponent, IngredientComponent, RecipeComponent, RecipeListComponent],
  imports: [
    CommonModule,
    MenuRoutingModule,
  ]
})
export class MenuModule { }
