import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.scss']
})
export class IngredientComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
