import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-ingredient-list',
  templateUrl: './ingredient-list.component.html',
  styleUrls: ['./ingredient-list.component.scss']
})
export class IngredientListComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
