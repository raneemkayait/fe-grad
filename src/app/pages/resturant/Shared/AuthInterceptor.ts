import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { first, mergeMap } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import * as fromAuth from '../auth/store/user.selector';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  re = 'Login';
  token = '';
  token$ = new Observable<string>();
  constructor(private store: Store<any>) {
  }
  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      // console.log(request.url);
      if (!request.url.includes(this.re)) {
        // console.log(' not Login request');
        return this.addToken(request).pipe(
            first(),
            mergeMap((requestWithToken: HttpRequest<any>) => next.handle(requestWithToken)),
        );
      }else {
        // console.log('Login request');
           return next.handle(request);
      }
}
private addToken(request: HttpRequest<any>): Observable<HttpRequest<any>> {
    // console.log('');
    return this.store.pipe(
        select(fromAuth.getUserToken),
        mergeMap((token: string) => {
                request = request.clone({
                    headers: request.headers.set('Authorization', `Bearer ${token}`),
                    withCredentials: true,
                });
            return of(request);
        }),
    );
}
}
