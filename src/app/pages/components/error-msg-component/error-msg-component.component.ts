import { Component, Input, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { LogOutTimerDialogComponent } from '../log-out-timer-dialog/log-out-timer-dialog.component';

@Component({
  selector: 'ngx-error-msg-component',
  templateUrl: './error-msg-component.component.html',
  styleUrls: ['./error-msg-component.component.scss']
})
export class ErrorMsgComponentComponent implements OnInit {

  constructor(protected dialogRef: NbDialogRef<ErrorMsgComponentComponent>) { }

  msg?: string;
  status: string;
  title: string;
  ngOnInit(): void {
    this.msg = this.msg || 'Some Unknown Error Occured Please Try Again';
    }
  close() {
    this.dialogRef.close();
  }
}
