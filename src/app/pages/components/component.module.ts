import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbAutocompleteModule, NbButtonModule, NbCardModule,
  NbCheckboxModule, NbDatepickerModule, NbIconModule, NbInputModule, NbPopoverModule,
  NbRadioModule, NbSelectModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NbEvaIconsModule } from '@nebular/eva-icons';
import { LogOutTimerDialogComponent } from './log-out-timer-dialog/log-out-timer-dialog.component';
import { ErrorMsgComponentComponent } from './error-msg-component/error-msg-component.component';
@NgModule({
  declarations: [ LogOutTimerDialogComponent,
    ErrorMsgComponentComponent],
  imports: [
    CommonModule,
    FormsModule,
    NbAutocompleteModule,
    NbInputModule,
    FormsModule,
    ReactiveFormsModule,
    NbPopoverModule,
    NbButtonModule,
    NbCardModule,
    NbCheckboxModule,
    NbEvaIconsModule,
    NbRadioModule,
    NbSelectModule,
    NbDatepickerModule,
    NbIconModule,
  ],
  exports: [
            LogOutTimerDialogComponent, ErrorMsgComponentComponent],
  providers: [
    { provide: 'componentName', useFactory: () => 'otherComponent'},
    ],
})
export class ComponentModule { }
