import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { Store } from '@ngrx/store';
// import * as AuthActions from '../../auth/store/user.actions';
@Component({
  selector: 'ngx-log-out-timer-dialog',
  templateUrl: './log-out-timer-dialog.component.html',
  styleUrls: ['./log-out-timer-dialog.component.scss'],
})
export class LogOutTimerDialogComponent implements OnInit {

  constructor(protected dialogRef: NbDialogRef<LogOutTimerDialogComponent>, private store: Store) { }

  ngOnInit(): void {
  }
  close () {
    this.dialogRef.close();
   // this.store.dispatch(new  AuthActions.CancelLogOutTimer());
  }
}
