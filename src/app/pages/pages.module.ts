import { NgModule } from '@angular/core';
import { NbMenuModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { ECommerceModule } from './e-commerce/e-commerce.module';
import { PagesRoutingModule } from './pages-routing.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { CommonModule } from '@angular/common';
import { AuthModule } from './resturant/auth/auth.module';
import { ComponentModule } from './components/component.module';

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    CommonModule,
    NbMenuModule,
    DashboardModule,
    ECommerceModule,
    MiscellaneousModule,
    AuthModule,
    ComponentModule
  ],
  declarations: [
    PagesComponent,
  ],
})
export class PagesModule {
}
