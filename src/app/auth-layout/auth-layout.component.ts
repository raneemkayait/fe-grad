import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-auth-layout',
  template:  `
  <ngx-auth>
    <router-outlet></router-outlet>
  </ngx-auth>
`,
  styleUrls: ['./auth-layout.component.scss']
})
export class AuthLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
