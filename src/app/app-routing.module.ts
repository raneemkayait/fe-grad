import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { LoginComponent } from './pages/resturant/auth/login/login.component';
import { AuthComponent } from './pages/resturant/auth/auth.component';
import { NbAuthComponent } from '@nebular/auth';
import { LoginGuard } from './pages/resturant/Services/login.guard';
import { AuthGuard } from './pages/resturant/Services/auth.guard';


export const routes: Routes = [
  {
    path: 'pages',
    canActivate: [AuthGuard],

    loadChildren: () => import('./pages/pages.module')
      .then(m => m.PagesModule),
  },
  {
    path: 'auth',
    canActivate: [LoginGuard],
    loadChildren: () => import('./pages/resturant/auth/auth.module')
      .then(m => m.AuthModule),
  },

  { path: '', redirectTo: 'pages', pathMatch: 'full' },
  { path: '**', redirectTo: 'pages' },
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
